package info.mindes.SimpleGrid;

public class GridTests {

    public static void main(String[] args) throws Exception {

        SimpleGrid grid = new SimpleGrid(2);
        grid.addEvent(new EventHandler() {
            
            @Override
            public void update(Position position) {
                System.out.println("update:"+position);
            }
            
            @Override
            public void unload(Position position) {
                System.out.println("unload:"+position);
            }
            
            @Override
            public void load(Position position) {
                System.out.println("load:"+position);
            }
        });

        grid.setOffset(5, 5);
//        grid.setCaching(false);

        grid.update(5, 5);
        if (!grid.getPosition().equals(new Position(0, 0))) {
            throw new Exception("Bad current tile {0, 0}:"+grid.getPosition());
        }

        grid.update(0, 0);
        if (!grid.getPosition().equals(new Position(-5, -5))) {
            throw new Exception("Bad current tile {-5, -5}:"+grid.getPosition());
        }

        Thread.sleep(6000);//to test caching 
        grid.update(1, 1);

        System.out.println("All tests OK");
    }
}

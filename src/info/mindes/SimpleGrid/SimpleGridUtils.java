package info.mindes.SimpleGrid;

/**
 * @author eraslt
 *
 */
public class SimpleGridUtils {

    public static Position getTileByPos(double x, double y, int unit_size) {
        return SimpleGridUtils.getTileByPos(x, y, unit_size, 0, 0);
    }

	public static Position getTileByPos(double x, double y, int unit_size, float offsetX, float offsetY) {
		x+=offsetX;
		y+=offsetY;
		return new Position((int) Math.floor(x/unit_size), (int) Math.floor(y/unit_size));
	}

	public static float[] getPosByTile(Position position, int unit_size) {
	    return SimpleGridUtils.getPosByTile(position,  unit_size, 0, 0);
	}

	public static float[] getPosByTile(Position position, int unit_size, float offsetX, float offsetY) {
		float x = (position.x * unit_size)  - offsetY;
		float y = (position.y * unit_size)  - offsetY;
		float[] r = {x, y, x+unit_size, y+unit_size, x+unit_size/2, y+unit_size/2};
		return r;
	}
}

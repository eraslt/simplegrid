package info.mindes.SimpleGrid;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author eraslt
 * 
 */
@SuppressWarnings("rawtypes")
public class SimpleGrid {

    private Position current = new Position(0, 0);
    private boolean valid = false;
    private int unit_size = 1;
    private int units;

    private float offsetX = 0;
    private float offsetY = 0;

    private boolean caching = true;
    private int cache_delay = 5000;

    private List<Position> positions = new ArrayList<>();
    private Map<Position, Long> cached = new HashMap<>();

    private List<EventHandler> eventHandlers = new ArrayList<>();

    private Position calcPosition(double x, double y) {
        int px = (int) Math.floor((x - offsetX) / unit_size);
        int py = (int) Math.floor((y - offsetY) / unit_size);
        return new Position(px, py);
    }

    /**
     * @param units - number of units to load from current grid
     */
    public SimpleGrid(int units) {
        this.units = units;
    }

    /**
     * @param EventHandler event
     */
    public void addEvent(EventHandler eventHandler) {
        eventHandlers.add(eventHandler);
    }

    /**
     * @param EventHandler[] events
     */
    public void addEvents(EventHandler... events) {
        for (int i = 0; i < events.length; i++) {
            eventHandlers.add(events[i]);
        }
    }

    /**
     * @param EventHandler event
     */
    public void removeEvent(EventHandler eventHandler) {
        if (eventHandlers.contains(eventHandler)) {
            eventHandlers.remove(eventHandler);
        }
    }

    /**
     * @param float x
     * @param float y - for 3d this z coord
     */
    public void setOffset(float x, float y) {
        offsetX = x;
        offsetY = y;
    }

    /**
     * @return integer - return current position
     */
    public Position getPosition() {
        return current;
    }

    /**
     * sets imaginable grid size in your imaginable units
     * 
     * @param grid_size - size in units
     */
    public void setGridSize(int grid_size) {
        this.unit_size = grid_size;
    }

    /**
     * Sets position and invalidates entire grid for regeneration
     * 
     * @param double x - position in imaginary units
     * @param double y - position in imaginary units(for 3d this is z coord)
     */
    public void setPosition(double x, double y) {
        setPosition(calcPosition(x, y));
    }

    /**
     * Sets position and invalidates entire grid for regeneration
     * 
     * @param Position - position
     */
    public void setPosition(Position position) {
        this.current = position;
        valid = false;
    }

    /**
     * @param double posX
     * @param double posY - for 3d this is z coord
     */
    public void update(double x, double y) {
        update(calcPosition(x, y));
    }

    /**
     * sets caching delay in miliseconds
     * 
     * @param int cache_delay - delay in miliseconds
     */
    public void setCacheDelay(int cache_delay) {
        this.cache_delay = cache_delay;
    }

    /**
     * enables or disables cache. if disabled units are removed instalntly on
     * update if enabled units are cached and not removed until provides delay
     * passes by, then removed in update loop
     * 
     * @param boolean caching
     */
    public void setCaching(boolean caching) {
        this.caching = caching;
    }

    public void update(Position position) {
        if (valid && position.equals(current))
            return; // not moved
        current = position;

        int dist;
        for (int i = 0; i < positions.size(); i++) {
            Position p = positions.get(i);
            dist = Math.max(Math.abs(p.x - current.x), Math.abs(p.y - current.y));
            if (dist > units) {
                if (caching && !cached.containsKey(p)) {
                    cached.put(p, System.currentTimeMillis());
                } else {
                    for (EventHandler eh : eventHandlers) {
                        eh.unload(p);
                    }
                    positions.remove(i);
                }
                continue;
            }

            if (dist != p.dist) {
                p.dist = dist;
                for (EventHandler eh : eventHandlers) {
                    eh.update(p);
                }
            }
        }

        for (int x = current.x - units; x <= current.x + units; x++) {
            for (int y = current.y - units; y <= current.y + units; y++) {
                dist = Math.max(Math.abs(x - current.x), Math.abs(y - current.y));

                if (valid && dist < units)
                    continue;// optimizition, skips inner units

                Position p = new Position(x, y, dist);
                if (!positions.contains(p)) {
                    positions.add(p);
                    for (EventHandler eh : eventHandlers) {
                        eh.load(p);
                    }
                } else {
                    //remove from delete list if we are tryinng to load it again
                    if (caching && cached.containsKey(p)) {
                        cached.remove(p);
                    }
                }
            }
        }

        // unload expired cached
        if (caching) {
            Iterator it = cached.entrySet().iterator();
            long now = System.currentTimeMillis();
            while (it.hasNext()) {
                Map.Entry pair = (Map.Entry) it.next();
                if ((((long) pair.getValue()) + cache_delay) < now) {
                    Position p = (Position) pair.getKey();
                    for (EventHandler eh : eventHandlers) {
                        eh.unload(p);
                    }
                    positions.remove(p);
                    it.remove(); // avoids a ConcurrentModificationException
                }
            }
        }

        valid = true;
    }
}

package info.mindes.SimpleGrid;

public interface EventHandler {

	/**
	 * @param Position position - position of tile beeing loaded
	 */
	public void load(Position position);

	/**
	 * @param Position position - position of tile beeing updated
	 */
	public void update(Position position);

	/**
	 * @param Position position - position of tile beeing unloaded
	 */
	public void unload(Position position);

}

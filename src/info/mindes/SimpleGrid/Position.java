package info.mindes.SimpleGrid;

import java.io.Serializable;

public class Position implements Serializable {

    private static final long serialVersionUID = 1L;
    public int x;
    public int y;
    public int dist = 0;

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + x;
        result = prime * result + y;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Position other = (Position) obj;
        if (x != other.x)
            return false;
        if (y != other.y)
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Position [x=" + x + ", y=" + y + ", dist=" + dist + "]";
    }

    public Position(int x, int y) {
        this.x = x;
        this.y = y;
    }
    
    public Position(int x, int y, int dist) {
        this.x = x;
        this.y = y;
        this.dist = dist;
    }
    
    public void setDist(int dist) {
        this.dist = dist;
    }
    
    public int getDist() {
        return dist;
    }
}
